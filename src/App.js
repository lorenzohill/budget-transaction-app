import Button from "@mui/material/Button";
import * as React from "react";
import dayjs from "dayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import "./App.css";
import {
  Box,
  Checkbox,
  FormControl,
  MenuItem,
  Select,
  TextField,
  FormControlLabel,
  InputLabel,
} from "@mui/material";

function App() {
  var toObject = require("dayjs/plugin/toObject");
  dayjs.extend(toObject);

  const [total, setTotal] = React.useState("");
  const [cat, setCat] = React.useState("");
  const [date, setDate] = React.useState(dayjs());
  const [desc, setDesc] = React.useState("");
  const [error, setError] = React.useState(false);

  function handleTotal(e) {
    setTotal(e);
  }

  function handleError() {
    if (error) {
      setError(false);
    } else {
      setError(true);
    }
  }

  const logTransaction = (e) => {
    e.preventDefault();
    const regex = /^\d+(\.\d{1,2})?$/; // pattern for valid monetary value
    const totalCheck = total;

    if (regex.test(totalCheck)) {
      console.log("Regex Pass");
      handleError();
      return console.log({
        Date: date.toObject(),
        Total: total,
        Description: desc,
        Category: cat,
      });
    } else {
      console.log("Regex Failed");
      handleError();
      return;
    }
  };

  const handleChange = (event) => {
    setCat(event.target.value);
  };

  return (
    <div className="App">
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Box className="appBox">
          <form onSubmit={(e) => logTransaction(e)}>
            <FormControl className="form">
              <DatePicker
                label="Basic date picker"
                value={date}
                onChange={(e) => setDate(date)}
              />
              <div className="totalAmountSplit">
                <TextField
                  label="Total Amount"
                  error={error}
                  variant="outlined"
                  id="total"
                  value={total}
                  onChange={(e) => handleTotal(e.target.value)}
                  helperText={error ? "Error: Please enter a vaild amount" : ""}
                />
                <FormControlLabel
                  control={<Checkbox />}
                  label="Split Up to multiple categories"
                />
              </div>

              <TextField
                label="Description"
                variant="outlined"
                onChange={(e) => setDesc(e.target.value)}
              />
              <FormControl>
                <InputLabel id="category-label">Category</InputLabel>
                <Select
                  labelId="category-label"
                  label="Category"
                  id="category"
                  onChange={handleChange}
                  value={cat}
                >
                  <MenuItem value={"Gas"}>Gas</MenuItem>
                  <MenuItem value={"Bills"}>Bills</MenuItem>
                  <MenuItem value={"Utility"}>Utility</MenuItem>
                  <MenuItem value={"Grocery"}>Grocery</MenuItem>
                </Select>
              </FormControl>

              <Button variant="contained" type="submit">
                Log Transaction
              </Button>
            </FormControl>
          </form>
        </Box>
      </LocalizationProvider>
    </div>
  );
}

export default App;
